#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <debug_utilities.h>
#include <time.h>


/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

#define BUFFER_SIZE 400

int main(int argc, char * argv []) {
  clock_t start = clock();

  configuration conf = {};
  for (size_t i = 1; i < argc; i++) {
      if (strcmp(argv[i],"-t")==0){
          conf.cache_size = atoi(argv[i+1]);
      }else if (strcmp(argv[i],"-l")==0){
          conf.block_size = atoi(argv[i+1]);
      }else if (strcmp(argv[i],"-a")==0){
          conf.associativity = atoi(argv[i+1]);
      }else if (strcmp(argv[i],"-rp")==0){
          conf.replacement_policy = atoi(argv[i+1]);
      }
  }

struct entry cache_line[2];
  operation_result result = {};

  int i;
    /* Fill cache line */
    for ( i =  0; i < 2; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (2 <= 2)? rand()%2: 3;
      while (cache_line[i].tag == 426) {
        cache_line[i].tag = rand()%4096;
      }
    }
    stats sim_stats = simulate(stdin,conf);
    
    // Print cache configuration 
    print_configuration(conf);
  
    // Print Statistics 
    print_stats(sim_stats);


  printf("Tiempo transcurrido: %f\n", ((double)clock() - start) / CLOCKS_PER_SEC);
return 0;
}
