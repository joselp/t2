/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>

typedef unsigned int u_int;
#define KB 1024
#define ADDRSIZE 32
#define BUFFER_SIZE  400
#define RP_VICTIMIZATION_VALUE 3
using namespace std;

void update_rp_value_rrip(entry* cache_block, int associativity,int* candidate_vic, int max_rp_value){
   int add = 3-max_rp_value;
   for(size_t i = 0; i < associativity; i++){
      cache_block[i].rp_value += add;
      if(cache_block[i].rp_value == 3){
         *candidate_vic = i;
      }
   }
}

void update_rp_value_rrip_hp(entry* cache_block, int associativity){
   for(size_t i = 0; i < associativity; i++){
      cache_block[i].rp_value++;
   }
}
void update_rp_value_lru(entry* cache_block, int associativity, int threshold){
   for(size_t i = 0; i < associativity; i++){
      if(cache_block[i].rp_value < threshold){
         cache_block[i].rp_value++;
      }
   }
}

void update_rp_value_nru(entry* cache_block, int associativity){
   for(size_t i = 0; i < associativity; i++){
      cache_block[i].rp_value = 1;
   }
}

void update_entry(entry* entry, int tag,int loadstore, int rp_value){
   if(loadstore == STORE){
      entry->dirty = true;
   }else{
      if(entry->tag != tag){
         entry->dirty = false;
      }
   }
   entry->valid    = true;
   entry->tag      = tag;
   entry->rp_value = rp_value; 
}

void update_stats(stats* stats,operation_result* results){
   switch (results->miss_hit)
   {
   case MISS_LOAD:
      stats->load_misses++;
      break;
   case MISS_STORE:
      stats->store_misses++;
      break;
   case HIT_LOAD:
      stats->load_hit++;
      break;
   case HIT_STORE:
      stats->store_hit++;
      break;
   default:
      break;
   }
   if(results->dirty_eviction){
      stats->dirty_evictions++;
   }
}

void print_configuration(configuration conf){
   string policy;
   switch (conf.replacement_policy){
      case LRU:    policy = "LRU\0"   ;break;
      case NRU:    policy = "NRU\0"   ;break;
      case RRIPFP: policy = "RRIPFP\0";break;
      case RRIPHP: policy = "RRIPHP\0";break;
      default: break;
   }
   printf("#############################################\n");
   printf("  Cache parameters\n");
   printf("#############################################\n");
   std::cout <<"Cache replacement policy:\t " +policy <<std::endl;
   printf(" Cache size (kB):\t\t %i\n",conf.cache_size);
   printf(" Cache associativity:\t\t %i\n",conf.associativity);
   printf(" Cache block size (bytes):\t %i\n",conf.block_size);
}

void print_stats(stats stats){
   int total_misses = stats.load_misses + stats.store_misses;
   int total_hit    = stats.load_hit + stats.store_hit;
   int total_access = total_hit + total_misses;
   int load_access = stats.load_misses + stats.load_hit;
   printf("############################################\n");
   printf("  Simulation Results\n");
   printf("############################################\n");
   printf(" Overall miss rate:\t\t %f\n",(double)total_misses/total_access);
   printf(" Read miss rate:\t\t %f\n",(double)stats.load_misses/load_access);
   printf(" Dirty evictions:\t\t %i\n",stats.dirty_evictions);
   printf(" Load misses:\t\t\t %i\n",stats.load_misses);
   printf(" Store misses:\t\t\t %i\n",stats.store_misses);
   printf(" Total misses:\t\t\t %i\n",total_misses);
   printf(" Load hits:\t\t\t %i\n",stats.load_hit);
   printf(" Store hits:\t\t\t %i\n",stats.store_hit);
   printf(" Total hits:\t\t\t %i\n",total_hit);
   printf("############################################\n");
}

void print_entry(entry set,const char* msj=""){
   printf("%s{%i,%i,%i} ",msj,set.tag,set.dirty,set.rp_value);
}

void print_set(entry* set, int assoc, const char* msj=""){
   printf("%s",msj);
   for(size_t i = 0; i < assoc; i++){
      print_entry(set[i]);
   }
   printf("\n");
}

void print_result(operation_result results,const char* msj=""){
   printf("%s{%i,%i,%i}\n",msj,results.miss_hit,results.dirty_eviction,results.evicted_address);
}

int log_2(int num){
   int res = 0;
   int div = num;
   while(div != 1){
    div = div >> 1;
    res ++;
   }
   return res;
}

bool is_pow2(int num){
   return !((bool)(num & (num-1)));
}

void process_line(char* line,long* address,int* loadstore){
	char buffer[400];
   strcpy(buffer, line);
	int cont = 0;
	char *token = strtok(buffer," ");
	while(token != NULL){
	   if(cont == 1){
	   	*loadstore = atoi(token);								
	   }
	   if(cont == 2){
	   	*address   = strtol(token,NULL,16);
	   }
	   token = strtok(NULL," ");
	   cont ++;
	}		
}

stats simulate(FILE* file,configuration conf){ 
   long address;
   stats sim_stats = {};
   char line[BUFFER_SIZE];
   int tag_size,idx_size,offset_size;
   int idx,tag,loadstore;
   field_size_get(conf.cache_size,conf.associativity,conf.block_size,&tag_size,&idx_size,&offset_size);
   entry  cache_blocks[1<<idx_size][conf.associativity] = {};
   while(fgets(line, BUFFER_SIZE, file) != NULL){
      process_line(line, &address, &loadstore);
      address_tag_idx_get(address, tag_size, idx_size, offset_size, &idx, &tag);
      operation_result results = {};
      switch (conf.replacement_policy)
      {
         case LRU:
            lru_replacement_policy(idx,tag,conf.associativity,loadstore,cache_blocks[idx],&results,false); 
            break;
         case NRU:
            nru_replacement_policy(idx,tag,conf.associativity,loadstore,cache_blocks[idx],&results,false);
            break;
         case RRIPHP:
            srrip_replacement_policy_hp(idx,tag,conf.associativity,loadstore,cache_blocks[idx],&results,false);
            break;
         case RRIPFP:
            srrip_replacement_policy_fp(idx,tag,conf.associativity,loadstore,cache_blocks[idx],&results,false);
            break;
         default:
            break;
      }
      update_stats(&sim_stats,&results);
   }
   fclose(file);  
   return sim_stats;
}

int field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *tag_size,
                    int *idx_size,
                    int *offset_size)
{

   *offset_size = log_2(blocksize_bytes);
   *idx_size    = log_2((cachesize_kb*(1024))/(associativity*blocksize_bytes));
   *tag_size    = ADDRSIZE - (*idx_size + *offset_size);
   return OK;
}

void address_tag_idx_get(long address,
                        int tag_size,
                        int idx_size,
                        int offset_size,
                        int *idx,
                        int *tag)
{
   *idx = (address >> offset_size) & ((1<<idx_size)-1);
   *tag =  address >> (idx_size + offset_size);
}

int srrip_replacement_policy_hp (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks, 
                             operation_result* result,
                             bool debug)
{
   if(idx < 0 || tag < 0 || associativity < 1 || !is_pow2(associativity)){
      return PARAM;
   }
   if(debug)printf("\n[access: %i]\n", tag);
   int way_free      = -1; 
   int candidate_vic = -1;
   int max_rp_value  = -1;
   for(size_t i = 0; i < associativity; i++){                                 //Check all the ways
      if(cache_blocks[i].valid == 1){                                         //Check if data valid on this way
         if(cache_blocks[i].tag == tag){                                      //Check hit
            result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
            update_entry(&cache_blocks[i], tag,loadstore,0);
            if(debug){
               print_set(cache_blocks,associativity);
               print_result(*result,"result : ");
            }
            return OK; 
         }
         if(cache_blocks[i].rp_value == RP_VICTIMIZATION_VALUE){                                   //If data in way with rp=3 its the vict candidate
            candidate_vic = i;
         }
         if(cache_blocks[i].rp_value > max_rp_value){
            max_rp_value = cache_blocks[i].rp_value;
         }
      }else{
         way_free = i;                                                        //If data invalid there its a free way
      }
   }
   result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
   if(way_free != -1){
      int rp_v = (associativity > 2)? 2:0;                                   //No free way in cache
      update_entry(&cache_blocks[way_free], tag,loadstore,rp_v);
      if(debug){
         print_set(cache_blocks,associativity);
         print_result(*result,"result : ");
      }
      return OK;
   }else{
      if(candidate_vic == -1){                                                 //No victimization candidate
         update_rp_value_rrip(cache_blocks,associativity,&candidate_vic,max_rp_value);
      }
      result->evicted_address = cache_blocks[candidate_vic].tag;
      result->dirty_eviction  = cache_blocks[candidate_vic].dirty;
      if(debug){ 
         print_entry(cache_blocks[candidate_vic],"victimized entry: ");
         printf("\n");
      }
      int rp_v = (associativity > 2)? 2:0;    
      update_entry(&cache_blocks[candidate_vic],tag,loadstore,rp_v);
      if(debug){
         print_set(cache_blocks,associativity);
         print_result(*result,"result : ");
      }
      return OK;
   }
   return ERROR;
}

int srrip_replacement_policy_fp (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks, 
                             operation_result* result,
                             bool debug)
{
   if(idx < 0 || tag < 0 || associativity < 1 || !is_pow2(associativity)){
      return PARAM;
   }
   if(debug)printf("\n[access: %i]\n", tag);
   int way_free      = -1;
   int candidate_vic = -1;
   int max_rp_value  = -1;
   for(size_t i = 0; i < associativity; i++){                                 //Check all the ways
      if(cache_blocks[i].valid == 1){                                         //Check if data valid on this way
         if(cache_blocks[i].tag == tag){                                      //Check hit
            result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
            int rp = cache_blocks[i].rp_value -1;
            rp = (rp < 0)? 0:rp;
            update_entry(&cache_blocks[i], tag,loadstore,rp);
            if(debug){
               print_set(cache_blocks,associativity);
               print_result(*result,"result : ");
            }
            return OK; 
         }
         if(cache_blocks[i].rp_value == RP_VICTIMIZATION_VALUE){                                   //If data in way with rp=3 its the vict candidate
            candidate_vic = i;
         }
         if(cache_blocks[i].rp_value > max_rp_value){
            max_rp_value = cache_blocks[i].rp_value;
         }
      }else{
         way_free = i;                                                        //If data invalid there its a free way
      }
   }
   result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
   if(way_free != -1){
      int rp_v = (associativity > 2)? 2:0;                                   //No free way in cache
      update_entry(&cache_blocks[way_free], tag,loadstore,rp_v);
      if(debug){
         print_set(cache_blocks,associativity);
         print_result(*result,"result : ");
      }
      return OK;
   }else{
      if(candidate_vic == -1){                                                 //No victimization candidate
         update_rp_value_rrip(cache_blocks,associativity,&candidate_vic,max_rp_value);
      }
      result->evicted_address = cache_blocks[candidate_vic].tag;
      result->dirty_eviction  = cache_blocks[candidate_vic].dirty;
      if(debug){ 
         print_entry(cache_blocks[candidate_vic],"victimized entry: ");
         printf("\n");
      }
      int rp_v = (associativity > 2)? 2:0; 
      update_entry(&cache_blocks[candidate_vic],tag,loadstore,rp_v);
      if(debug){
         print_set(cache_blocks,associativity);
         print_result(*result,"result : ");
      }
      return OK;
   }
   return ERROR;
}

int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks, 
                             operation_result* result,
                             bool debug)
{
   if(idx < 0 || tag < 0 || associativity < 1 || !is_pow2(associativity)){
      return PARAM;
   }
   if(debug)printf("\n[access: %i]\n", tag);
   int way_free      = -1;
   int candidate_vic = -1;
   for(size_t i = 0; i < associativity; i++){
      if(cache_blocks[i].valid == 1){
         result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
         if(cache_blocks[i].tag == tag){
            int threshold = cache_blocks[i].rp_value;
            update_entry(&cache_blocks[i], tag,loadstore,-1);
            update_rp_value_lru(cache_blocks,associativity,threshold);
            if(debug){
               print_set(cache_blocks,associativity);
               print_result(*result,"result : ");
            }
            return OK; 
         }
         if(cache_blocks[i].rp_value == associativity-1){
            candidate_vic = i;
         }
      }else{
         way_free = i;
      }
   }
   result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
   if(way_free != -1){
      update_entry(&cache_blocks[way_free], tag,loadstore,-1);
      update_rp_value_lru(cache_blocks,associativity,associativity-1);
      if(debug){
         print_set(cache_blocks,associativity);
         print_result(*result,"result : ");
      }
      return OK;
   }else{
      result->evicted_address = cache_blocks[candidate_vic].tag;
      result->dirty_eviction  = cache_blocks[candidate_vic].dirty;
      if(debug){ 
         print_entry(cache_blocks[candidate_vic],"victimized entry: ");
         printf("\n");
      }
      int threshold = cache_blocks[candidate_vic].rp_value;
      update_entry(&cache_blocks[candidate_vic],tag,loadstore,-1);
      update_rp_value_lru(cache_blocks,associativity,threshold);
      if(debug){
         print_set(cache_blocks,associativity);
         print_result(*result,"result : ");
      }
      return OK;
   }
   return ERROR;
}

int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* result,
                           bool debug)
{
   if(idx < 0 || tag < 0 || associativity < 1 || !is_pow2(associativity)){
      return PARAM;
   }
   if(debug)printf("\n[access: %i]\n", tag);
   int way_free      = -1;
   int candidate_vic = -1;
   for(size_t i = 0; i < associativity; i++){
      if(cache_blocks[i].valid == 1){
         if(cache_blocks[i].tag == tag){
            result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
            update_entry(&cache_blocks[i], tag,loadstore,0);
            if(debug){
               print_set(cache_blocks,associativity);
               print_result(*result,"result : ");
            }
            return OK; 
         }
         if(cache_blocks[i].rp_value == 1){
            candidate_vic = i;
         }
      }else{
         way_free = i;
      }
   }
   result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
   if(way_free != -1){
      update_entry(&cache_blocks[way_free], tag,loadstore,0);
      if(debug){
         print_set(cache_blocks,associativity);
         print_result(*result,"result : ");
      }
      return OK;
   }else{
      if(candidate_vic == -1){
         update_rp_value_nru(cache_blocks,associativity);
         candidate_vic = associativity-1;
      }
      result->evicted_address = cache_blocks[candidate_vic].tag;
      result->dirty_eviction  = cache_blocks[candidate_vic].dirty;
      if(debug){ 
         print_entry(cache_blocks[candidate_vic],"victimized entry: ");
         printf("\n");
      }
      update_entry(&cache_blocks[candidate_vic],tag,loadstore,0);
      if(debug){
         print_set(cache_blocks,associativity);
         print_result(*result,"result : ");
      }
      return OK;
   }
   return ERROR;
}