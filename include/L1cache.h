/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#ifndef L1CACHE_H
#define L1CACHE_H

#include <netinet/in.h> 
/* 
 * ENUMERATIONS 
 */

/* Return Values */
enum returns_types {
 OK,
 PARAM,
 ERROR
};

/* Represent the cache replacement policy */
enum replacement_policy{
 LRU,
 NRU,
 RRIPHP,
 RRIPFP,
 RANDOM 
};

enum miss_hit_status {
 MISS_LOAD,
 MISS_STORE,
 HIT_LOAD,
 HIT_STORE
};

enum load_store{
    LOAD,
    STORE
};

/*
 * STRUCTS
 */

/* Cache tag array fields */
struct entry {
    bool valid;
    bool dirty;
    int  tag;
    int8_t rp_value; // cambio
};

/* Cache replacement policy results */
typedef struct{
    enum miss_hit_status miss_hit;
    bool dirty_eviction;
    int  evicted_address;
}operation_result;

typedef struct{
    int cache_size;
    int block_size;
    int associativity;
    int replacement_policy;
}configuration;

typedef struct{
    int cache_size;
    int associtivity;
    int block_size;
    int dirty_evictions;
    int load_misses;
    int store_misses;
    int load_hit;
    int store_hit;
}stats;

/* 
 *  Functions
 */


void update_rp_value_lru(entry* cache_block, int associativity, int threshold);

/*
 * Update cache values for rrip replacement policy
 * ADD 1 to rp_value on each way
 * 
 * [in] cache_block: A set of the cache
 * [in] associativity: number of ways of the cache
 */

void update_rp_value_rrip(entry* cache_block, int associativity);
/*
*  Update cache entry values when a block is replaced
* [in] entry: reference to an entry of cache
* [in] tag: new value of tag
* [in] loadsrore: identifier for a type of entry (load or store) needed to update the dirty bit
* [in] rp_value: New value of the rp number
*/
void update_entry(entry* entry, int tag, int loadstore, int rp_value);
/**
 * Update results of dirty bit, evicted tag and miss-hit counters
 * [in] stats: reference to struct of stats 
 * [in] results: reference to struct of results
 **/
void update_stats(stats* stats,operation_result* results);
/**
 * Print a summary of inputs to simulate
 * [in] conf: struct with input configuration info
 **/
void print_configuration(configuration conf);

/**
 * Print a summary of results
 * [in] stats: struct with results info
 **/
void print_stats(stats stats);

/**
 * Print an especific set of cache
 * [in] set: reference to the first position of the set
 * [in] assoc: associativity, number of ways of the set
 * [in] msj: empty string to concatenate entry info into a string array 
 **/
void print_set(entry* set, int assoc, const char* msj);

/* 
 * Get an log_2 of a number with multiplicity of 2
 * [in] num: numer
 * [out] result: result of the log_2
 */
int log_2(int num);



bool is_pow2(int num);
/* 
 * Get address from input line
 * [in] line: input line from trace file
 * [in] address: full address
 * [in] loadstore: indicate if this acces its load or store from input
 */
void process_line(char* line,long* address,int* loadstore);

/**
 * Simulate cache update process
 * [in] file: compress file with instructions info
 * [in] conf: struct with input configuration info
 * [out] sim_stats: struct of stats with final results
 **/

stats simulate(FILE* file,configuration conf);

/*
 * Get tag, index and offset length
 * 
 * [in] cache_size: total size of the cache in Kbytes
 * [in] associativity: number of ways of the cache
 * [in] blocksize_bytes: size of each cache block in bytes
 *
 * [out] tag_size: size in bits of the tag field
 * [out] idx_size: size in bits of the index field
 * [out] offset_size: size in bits of the offset size
 */
int field_size_get(int cachesize_kb,
                   int associativity,
                   int blocksize_bytes,
                   int *tag_size,
                   int *idx_size,
                   int *offset_size);

/* 
 * Get tag and index from address
 * 
 * [in] address: memory address
 * [in] tag_size: number of bits of the tag field
 * [in] idx_size: number of bits of the index field
 *
 * [out] idx: cache line idx
 * [out] tag: cache line tag
 */

void address_tag_idx_get(long address,
                         int tag_size,
                         int idx_size,
                         int offset_size,
                         int* idx,
                         int* tag);


/* 
 * Search for an address in a cache set and
 * replaces blocks using SRRIP(hp) policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation false if load true if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int srrip_replacement_policy_hp (int idx,
                              int tag,
                              int associativity,
                              bool loadstore,
                              entry* cache_blocks,
                              operation_result* operation_result,
                              bool debug=false);

/* 
 * Search for an address in a cache set and
 * replaces blocks using LRU policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation false if true if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int lru_replacement_policy (int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug=false);

/* 
 * Search for an address in a cache set and
 * replaces blocks using NRU policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation false if true if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
*/

int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug=false);

/* 
 * Search for an address in a cache set and
 * replaces blocks using SRRIP(fp) policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation false if load true if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */

int srrip_replacement_policy_fp(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug=false);




#endif
