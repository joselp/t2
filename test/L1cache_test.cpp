/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#define YEL   "\x1B[33m"
#include "iostream"

bool is_in_set(entry* set, int assoc, int tag){
  for (size_t i = 0; i < assoc; i++){
    if(set[i].tag == tag){
      return true;
    }
  }
  return false;
}

/* Globals */
int debug_on = 0;

/* Test Helpers */
#define DEBUG(x) if (debug_on) printf("%s\n",#x)
/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (bool (debug_on)) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity] = {};
  /* Check for a miss */
  DEBUG(Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    if(bool (debug_on)) print_set(cache_line,associativity,"");
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy_hp(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy_hp(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

TEST(L1cache, hit_miss_rrip_fp){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 1;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (bool (debug_on)) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  
  struct entry cache_line[associativity] = {};
  /* Check for a miss */
  DEBUG(Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    if(bool (debug_on)) print_set(cache_line,associativity,"");
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy_fp(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy_fp(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST(L1cache, hit_miss_lru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  operation_result result = {};
 
  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (bool (debug_on)) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  struct entry cache_line[associativity] = {};
  /* Check for a miss */
  DEBUG(Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    if(bool (debug_on)) print_set(cache_line,associativity,"");
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST(L1cache, hit_miss_nru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (bool (debug_on)) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity] = {};
  /* Check for a miss */
  DEBUG(Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = 0;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    if(bool (debug_on)) print_set(cache_line,associativity,"");
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST(L1cache, promotion){
  int status;
  int replacement_policy;
  int (*func_policy)(int,int,int,bool,entry*,operation_result*,bool);
  int expected_rp_value;
  bool expected_evicted_address;
  operation_result result = {};
  // Choose a random policy 
  replacement_policy = rand()%4;
  switch (replacement_policy){
    case LRU:    func_policy = lru_replacement_policy;      break;
    case NRU:    func_policy = nru_replacement_policy;      break;
    case RRIPFP: func_policy = srrip_replacement_policy_fp; break;
    case RRIPHP: func_policy = srrip_replacement_policy_hp; break;
    default: break;
  }
  /* Choose a random associativity */
  int associativity = 1 << (rand()%4);
  /* Bloque A */
  int idx_A = rand()%1024;
  int tag_A = rand()%4096;
  if (debug_on) {
    printf("policy: %i\n",replacement_policy);
    printf("assoc:  %i\n",associativity);
    printf("tag_A:  %i\n",tag_A);
    printf("\n->Filling cache\n");
  }
  // Fill a cache entry with only read operations
  struct entry cache_line[associativity] = {};
  for (int i =  0; i < associativity; i++) {
    int idx = rand()%1024;
    int tag = rand()%4096;
    while(tag == tag_A || is_in_set(cache_line,associativity,tag)){
      tag = rand()%4096;
    }
    status = func_policy(idx,tag, associativity,false,cache_line,&result,bool(debug_on));
    EXPECT_EQ(status, 0);
  }
  if (debug_on) printf("\n->Start test\n");
  /***************************************************************************************/
  DEBUG(Check rp_value of block A);
  // Insert a new block A
  status = func_policy(idx_A,tag_A, associativity,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, 0);
  // Force a hit on A
  status = func_policy(idx_A,tag_A, associativity,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, 0);
  // Check rp_value of block A
  DEBUG(Checking rp_value of block A);
  switch (replacement_policy){
    case LRU:    expected_rp_value = 0;break;
    case NRU:    expected_rp_value = 0;break;
    case RRIPFP: expected_rp_value = (associativity > 2)? 1:0;break;
    case RRIPHP: expected_rp_value = 0;break;
    default: printf("ERROR\n"); break;
  }
  int rp_A;
  for(size_t i = 0; i < associativity; i++){
    if(cache_line[i].tag == tag_A){
      rp_A = cache_line[i].rp_value;
      break;
    }
  }
  EXPECT_EQ(rp_A,expected_rp_value);
  /***************************************************************************************/
  DEBUG(Check eviction of block A happen after N new blocks were inserted);
  int N;
  switch (replacement_policy){
    case RRIPHP: N = (associativity > 2)? (3*associativity)-2:associativity; break;
    case RRIPFP: N = (associativity > 2)? (2*associativity)-1:associativity; break;
    default:     N = associativity;     break;
  }
  // Keep inserting new blocks until A is evicted
  for(size_t  i = 0; i < N; i++){
    int tag = rand()%4096;
    int idx = rand()%1024;
    while(is_in_set(cache_line,associativity,tag)){
      tag = rand()%4096;
    }
    status = func_policy(idx,tag,associativity,false,cache_line,&result,bool(debug_on));
    EXPECT_EQ(status, 0);
  }
  expected_evicted_address = tag_A;
  EXPECT_EQ(result.evicted_address, tag_A);
}

/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST(L1cache, writeback){
  int status;
  int replacement_policy;
  int (*func_policy)(int,int,int,bool,entry*,operation_result*,bool);
  bool expected_dirty_eviction;
  operation_result result = {};
  // Choose a random policy 
  replacement_policy = rand()%4;
  int a;
  switch (replacement_policy){
    case LRU:    func_policy = lru_replacement_policy;      break;
    case NRU:    func_policy = nru_replacement_policy;      break;
    case RRIPFP: func_policy = srrip_replacement_policy_fp; break;
    case RRIPHP: func_policy = srrip_replacement_policy_hp; break;
    default: break;
  }
  /* Choose a random associativity */
  int associativity = (1 << (rand()%4));
  /* Bloque A */
  int idx_A = rand()%1024;
  int tag_A = rand()%4096;
  /* Bloque B */
  int idx_B = rand()%1024;
  int tag_B = rand()%4096;
  while (tag_B == tag_A)
  {
    tag_B = rand()%4096;
  }
  if (debug_on) {
    printf("policy: %i\n",replacement_policy);
    printf("assoc: %i\n",associativity);
    printf("tag_A: %i\n",tag_A);
    printf("tag_B: %i\n",tag_B);
    printf("\n->Filling cache\n");
  }
  // Fill a cache entry with only read operations
  struct entry cache_line[associativity] = {};
    for (int i =  0; i < associativity; i++) {
    int idx = rand()%1024;
    int tag = rand()%4096;
    while(is_in_set(cache_line,associativity,tag)) {
      tag = rand()%4096;
    }
    status = func_policy(idx,tag, associativity,false,cache_line,&result,bool(debug_on));
    EXPECT_EQ(status, 0);
  }
  if (debug_on) printf("\n->Start test\n");
  // Force a read hit for a random block B 
  status = func_policy(idx_B,tag_B, associativity,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, 0);
  status = func_policy(idx_B,tag_B, associativity,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, 0);
  /***************************************************************************************/
  DEBUG(Check dirty_bit for block B is false);

  // Insert lines until B is evicted 
  while (result.evicted_address != tag_B){
    int tag = rand()%4096;
    int idx = rand()%1024;
    while(tag == tag_A || tag == tag_B){
      tag = rand()%4096;
    }
    status = func_policy(idx,tag,associativity,false,cache_line,&result,bool(debug_on));
    EXPECT_EQ(status, 0);
  }
  // Check dirty_bit for block B is false
  expected_dirty_eviction  = false;
  EXPECT_EQ(result.dirty_eviction,expected_dirty_eviction);
  /***************************************************************************************/  
  // Force a write hit for a random block A
  status = func_policy(idx_A,tag_A, associativity,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, 0);
  status = func_policy(idx_A,tag_A, associativity,true,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, 0);
  // Force read hit for random block A 
  status = func_policy(idx_A,tag_A, associativity,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, 0);
  DEBUG(Check dirty_bit for block A is true);  
  // Insert lines until A is evicted 
  while (result.evicted_address != tag_A){
    int idx = rand()%1024;
    int tag = rand()%4096;
    while(tag == tag_A || tag == tag_B){
      tag = rand()%4096;
    }
    status = func_policy(idx,tag,associativity,false,cache_line,&result,bool(debug_on));
    EXPECT_EQ(status, 0);
  }
  // Check dirty_bit for block A is true 
  expected_dirty_eviction  = true;
  EXPECT_EQ(result.dirty_eviction,expected_dirty_eviction);
}

/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST(L1cache, boundaries){
  int status;
  int replacement_policy;
  int (*func_policy)(int,int,int,bool,entry*,operation_result*,bool);
  operation_result result = {};
  // Choose a random policy 
  replacement_policy = rand()%4;
  switch (replacement_policy){
    case LRU:    func_policy = lru_replacement_policy;      break;
    case NRU:    func_policy = nru_replacement_policy;      break;
    case RRIPFP: func_policy = srrip_replacement_policy_fp; break;
    case RRIPHP: func_policy = srrip_replacement_policy_hp; break;
    default: break;
  }
  int idx = -1*rand()%1024;
  while(idx < 0){idx = -1*rand()%1024;};
  int tag = -1*rand()%4096;
  while(tag < 0){tag = -1*rand()%1024;};
  int associativity = -1*(rand()%33);
  while(associativity < 1){associativity = -1*rand()%1024;};
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  struct entry cache_line[associativity] = {};
  /***************************************************************************************/
  DEBUG(Check function returns a PARAM error condition);
  status = func_policy(idx,tag,associativity,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, PARAM);
  status = func_policy(idx,tag,associativity,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, PARAM);
  status = func_policy(idx,tag,associativity,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, PARAM);
  while (is_pow2(associativity)){
    associativity = rand()%33;
  } 
  if(debug_on) printf("assoc: %i",associativity);
  status = func_policy(idx,tag,3,false,cache_line,&result,bool(debug_on));
  EXPECT_EQ(status, PARAM);
}

/* 
 * Gtest main function: Generates random seed, if not provided,
 * parses DEBUG flag, and execute the test suite
 */
int main(int argc, char **argv) {
  int argc_to_pass = 0;
  char **argv_to_pass = NULL; 
  int seed = 0;

  /* Generate seed */
  seed = time(NULL) & 0xffff;

  /* Parse arguments looking if random seed was provided */
  argv_to_pass = (char **)calloc(argc + 1, sizeof(char *));
  
  for (int i = 0; i < argc; i++){
    std::string arg = std::string(argv[i]);

    if (!arg.compare(0, 20, "--gtest_random_seed=")){
      seed = atoi(arg.substr(20).c_str());
      continue;
    }
    argv_to_pass[argc_to_pass] = strdup(arg.c_str());
    argc_to_pass++;
  }

  /* Init Gtest */
  ::testing::GTEST_FLAG(random_seed) = seed;
  testing::InitGoogleTest(&argc, argv_to_pass);

  /* Print seed for debug */
  printf(YEL "Random seed %d \n",seed);
  srand(seed);

  /* Parse for debug env variable */
  get_env_var("TEST_DEBUG", &debug_on);

  /* Execute test */
  return RUN_ALL_TESTS();
  
  /* Free memory */
  free(argv_to_pass);

  return 0;
}
